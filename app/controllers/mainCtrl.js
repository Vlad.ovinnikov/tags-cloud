(function() {

    MainCtrl = function ($scope, ArticleService) {
        console.log("MainCtrl2");

        $scope.textsList = [];
        $scope.articles = ArticleService.getArticles();
        $scope.articlesUnique = getUnique($scope.articles);
        
        $scope.getTexts = function(texts) {

            $scope.$apply(function() {
                $scope.textsList = texts;
            });
        }

        function getUnique(inputArray){
            var unique = {},
                uniqueList = [],
                tags = [],
                key ="name",
                i = 0;
            for(i; i < inputArray.length; i++){
                tags = inputArray[i].tags;
                angular.forEach(tags, function(value, key){
                    var res = value;

                    if (!unique[value]) {
                        uniqueList.push({name: value, quantity: 1});
                        unique[value] = value;
                        
                    } else {
                        angular.forEach(uniqueList, function(value, key){
                            if (uniqueList[key].name == res){
                                uniqueList[key].quantity++;
                            }
                        })
                    }        
                });
            }
            return uniqueList;
        }
    };
    
    MainCtrl.$inject = ['$scope', 'ArticleService'];
    var app = angular.module('tagscloud');
    app.controller('MainCtrl', MainCtrl);
    
    app.directive('linkSize', function($rootScope, SaveService){
        
        var template = '{{art.name}}';
            
        function link (scope, elem, attrs){
            var matchedTexts = [],
                selectedTags = [];
            
            $rootScope.matchedTexts = [];
            
            scope.$watch('art', function(art){
                elem.css('font-size', scope.art.quantity*4+'px');
            });
            
            addRemoveEvent();
            
            function addRemoveEvent() {
                elem.on('click', function() {
                    if (elem.hasClass('new-link-clicked')){
                        elem.removeClass('new-link-clicked');
                        matchedTexts = SaveService.remove(scope.datasource, scope.art);
                    } else {                        
                        elem.addClass('new-link-clicked');
                        matchedTexts = SaveService.add(scope.datasource, scope.art);
                    }
                    scope.add()(matchedTexts);
                    return matchedTexts;
                }); 
            } 
        }
        
        return {
            restrict: 'E',
            scope: {
                datasource: '=',
                art: '=',
                add: '&'
            },
            template: template,         
            link: link
        }
    });
})();