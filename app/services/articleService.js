(function(){

   var ArticleService = function(){
       var service = {},
           articles = {
          "articles":[  
      {  
         "text":"Lorem ipsum dolor sit amet augue sit amet diam sed elit odio, in augue. Fusce et odio. In et lorem. Morbi congue risus. Morbi tellus. Donec interdum euismod id, neque. Cras semper risus. Sed pulvinar nonummy sodales eros. Aliquam eleifend in, cursus arcu id lectus. Ut sit amet, diam. Vestibulum ante ipsum dolor sit amet nunc ultrices tincidunt, risus pede, luctus sagittis. Curabitur ultrices vel, purus. Nulla consectetuer, augue quis neque eu orci. Nullam justo a dolor. Morbi eleifend adipiscing posuere ante ipsum eget quam congue dolor. Suspendisse et ultrices posuere cubilia Curae, Phasellus sem. Integer a metus. Aliquam semper. Morbi pede. Sed nonummy dui.",
         "tags":[  
            "lorem",
            "ipsum",
            "lex",
            "cursus"
         ]
      },
      {  
         "text":"In pretium eget, aliquam ut, lobortis augue eu dui pulvinar gravida, erat volutpat. Vivamus faucibus orci vel quam. Nulla facilisi. Etiam sit amet, ligula. Aenean ac dolor. Nulla venenatis arcu. Sed quam sem, accumsan at, metus. Nulla in faucibus erat. Proin rhoncus non, tristique senectus et ultrices posuere cubilia Curae, In hac habitasse platea dictumst. Vestibulum ante sit amet magna. Aliquam vel risus. Ut bibendum sapien a nunc. Aliquam erat sed lacus. In hac habitasse platea dictumst. Suspendisse eu sem semper egestas. Suspendisse pede. Duis a pellentesque sapien. Morbi accumsan, libero at lobortis condimentum urna, egestas accumsan. In hac habitasse platea dictumst. Maecenas ante. Suspendisse lectus pharetra ut, gravida vitae, fringilla.",
         "tags":[  
            "lorem",
            "lex",
            "vitae",
            "felis"
         ]
      },
      {  
         "text":"Nunc massa et ultrices nulla. Ut vestibulum sapien, tempus vehicula, feugiat congue, sem ac magna neque, eu lacus. In hac habitasse platea dictumst. Cum sociis natoque penatibus et ultrices lorem nec massa. Mauris mattis ipsum dolor sit amet, sodales nibh quis felis augue egestas sit amet dolor. Nam sed felis auctor vulputate molestie. Nulla dignissim. Pellentesque sagittis, mi mauris, rutrum sit amet erat. Suspendisse elit. Lorem ipsum dolor sit amet, rutrum vel, arcu. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce condimentum urna, id nisl mollis aliquam, purus laoreet risus. Phasellus placerat consequat. Donec nec enim sit amet eros. Morbi eleifend in, cursus ut, pulvinar.",
         "tags":[  
            "felis",
            "cursus",
            "vitae",
            "elementum"
         ]
      },
      {  
         "text":"Cras vitae quam. Cum sociis natoque penatibus et lacus pede, molestie tincidunt. Maecenas tortor justo augue id nibh. Morbi id rutrum elementum. Fusce ligula. Cras ut massa. Vivamus ornare, erat blandit libero. Cum sociis natoque penatibus et interdum mi at justo. Maecenas at tortor. Proin vel pulvinar augue. Maecenas bibendum pede rhoncus tortor. Praesent consequat. Nulla ligula sit amet tellus. Morbi pede. Nullam in faucibus sit amet, sollicitudin fringilla. Morbi mattis sem. Suspendisse pede. Morbi porttitor, lacus ultrices posuere commodo, est eu lacus. Maecenas at sapien. Fusce blandit justo, condimentum sem eget metus bibendum leo elit eu viverra neque, mattis a, dolor. Fusce nisl ut massa. Vestibulum ante ipsum primis in faucibus et, mollis luctus. Nulla ante. Proin aliquam ut, laoreet molestie vitae, semper nisl, faucibus eros sed felis. Fusce dui.",
         "tags":[  
            "elementum",
            "lorem"
         ]
      },
      {  
         "text":"Praesent vitae mauris. Praesent blandit ultrices tincidunt, tempor vitae, ligula. Mauris pretium, diam ut tellus tortor, a urna. Donec eget orci dictum accumsan. Cras sit amet neque. Praesent nec felis auctor est, dapibus eu, egestas sodales, augue a dolor tincidunt leo lobortis non, dolor. Vestibulum et magnis dis parturient montes, nascetur ridiculus mus. Integer id nulla eu pede urna magna vitae lacus. Nullam tellus ac turpis non orci. Nunc arcu vitae lacus. Cras ac turpis in enim sit amet, accumsan et, congue fringilla tempus erat eu tortor ante sit amet, volutpat eu, fringilla condimentum, dapibus sit amet, vulputate faucibus. Sed nec nunc hendrerit sagittis. Aliquam risus commodo est pede, molestie elit, dictum sed, sollicitudin vitae, cursus vitae, cursus quis, congue odio eget elit tincidunt vel, varius dui. Morbi eget nibh vel tincidunt congue. Nunc mollis. Proin vitae porttitor auctor, sapien eget lectus felis vitae neque dui, accumsan sit amet, consectetuer.",
         "tags":[  
            "ipsum",
            "elementum",
            "vitae"
         ]
      },
      {  
         "text":"Proin nunc lacus, elementum in, libero. Aenean augue eu pede eu ante. Curabitur ut magna. Donec enim vulputate tempor magna hendrerit risus. Aliquam sem. Mauris in faucibus ipsum primis in posuere ligula in est. Maecenas pretium, ipsum pede, pulvinar interdum, tortor elit odio, in elit. Aenean ac turpis risus auctor odio. Donec nec scelerisque neque magna pharetra sem dolor ac dignissim a, euismod pulvinar, quam ultricies in, purus. Quisque rutrum, urna vitae pede. Cras aliquet. Morbi nibh ac ante pellentesque auctor varius. In tristique magna. Ut tempus vehicula, fringilla ut, blandit egestas ac, laoreet hendrerit feugiat augue eu nibh. Maecenas pharetra sem vitae massa sit amet elit eu augue nec massa. Ut sodales pede. Lorem ipsum adipiscing gravida iaculis ut, blandit vel, sapien. Morbi aliquam cursus mauris ac quam eu turpis faucibus orci eget felis. Donec lectus orci, aliquam ac, magna. Etiam imperdiet, nisl tristique ullamcorper. Nam varius justo.",
         "tags":[  
            "dolor",
            "sit",
            "amet"
         ]
      },
      {  
         "text":"Duis commodo pede placerat nec, lacinia varius et, tempus orci sit amet, consectetuer eget, cursus ligula felis, feugiat ultrices posuere cubilia Curae, Phasellus vestibulum. Pellentesque fringilla sit amet mauris mattis ac, pede. Mauris imperdiet aliquam enim non sem. Nulla eleifend justo non urna. Nam lectus rhoncus placerat ante. Nullam bibendum ac, euismod nulla id turpis nec leo. Suspendisse semper id, condimentum nunc. Vestibulum convallis non, iaculis augue. Vivamus justo. Integer non mi metus, quis metus. Curabitur eget sapien leo ut justo vel risus. Aliquam auctor congue ac, magna. Integer ac diam. Morbi placerat vehicula ut, nunc. Suspendisse luctus et eros pellentesque vel, metus. Nullam in wisi. Morbi pede. Donec nonummy at, vehicula libero laoreet hendrerit dolor urna, id dui. Morbi mauris.",
         "tags":[  
            "lex",
            "cursus"
         ]
      },
      {  
         "text":"Curabitur quam et accumsan at, quam. Maecenas eget libero auctor neque. Etiam semper, nunc sit amet pede. Etiam vestibulum et, imperdiet sed, imperdiet dignissim, sapien enim diam leo elit lectus eget velit. Suspendisse molestie. Donec aliquet eu, semper auctor. Maecenas wisi. Donec congue. Nam consectetuer arcu ac nulla. Sed ornare, odio et leo. Integer neque tristique magna porttitor lacus nonummy eget, sollicitudin magna. Vestibulum commodo tincidunt mauris. Praesent tortor. In ornare lorem. Praesent gravida hendrerit et, vehicula convallis pellentesque, wisi. Phasellus ipsum dolor sit amet, iaculis mi. Suspendisse elit iaculis ut, dignissim dolor sit amet dui non risus. Aliquam tempus enim vehicula ullamcorper, augue quis lacus. Integer eros diam felis, malesuada elit odio, a odio sagittis vel, viverra diam vel dolor. Fusce congue. Proin sodales. Aenean nonummy porttitor ullamcorper, augue id lectus. In urna. Phasellus blandit, enim sodales turpis, rutrum sit amet wisi. Nam hendrerit. Donec enim fringilla orci. Nunc.",
         "tags":[  
            "leo",
            "felis",
            "sit",
            "dolor"
         ]
      },
      {  
         "text":"Maecenas bibendum leo, cursus wisi a urna. Aenean posuere lobortis, mi id magna. Donec elementum vitae, sollicitudin eu, odio. Donec nec mauris. Nullam aliquet. Morbi commodo. Curabitur gravida vel, accumsan placerat urna semper quis, ipsum. Duis luctus, enim sapien facilisis libero. Cum sociis natoque penatibus et orci ac quam placerat quam. Nam varius in, iaculis augue. Nulla in eleifend erat erat ornare a, bibendum risus. Aliquam auctor ligula. Curabitur tempor. Phasellus vestibulum. Etiam et ultrices fringilla sed, euismod scelerisque rhoncus purus, pharetra volutpat. Pellentesque ac felis. Mauris rutrum, wisi sapien auctor consectetuer. Etiam varius, leo. Etiam ullamcorper, enim vel libero. Aliquam orci. Vestibulum tempus.",
         "tags":[  
            "lorem"
         ]
      },
      {  
         "text":"Mauris suscipit et, imperdiet dignissim vitae, ligula. Sed nec nibh consectetuer adipiscing arcu nibh porta tincidunt. Maecenas scelerisque, dui odio orci, gravida diam. Etiam vehicula lectus. Nam consectetuer arcu nec ante. Pellentesque ante. Morbi augue id rutrum pede tortor non eros. In ornare velit nulla ac erat. Quisque urna. Suspendisse justo orci convallis varius. Morbi mauris non ipsum. Vestibulum dolor accumsan rutrum, wisi accumsan quam, ultrices ut, nonummy consequat. Quisque orci. Phasellus posuere consectetuer. Etiam accumsan placerat id, turpis. Pellentesque sagittis, mi metus, quis viverra lacinia. Vestibulum ante in interdum adipiscing felis sit amet risus. Etiam aliquam eros cursus vitae, ultricies viverra neque, vitae ornare lobortis velit in nulla eu diam. Morbi.",
         "tags":[  
            "felis",
            "dolor",
            "sit"
         ]
      },
      {  
         "text":"Pellentesque at eros. Mauris rutrum, libero mollis eu, fringilla eget, dapibus mauris quis massa vel sodales in, odio. Morbi scelerisque pede dictum id, elit. Aliquam erat sed viverra diam in nunc. Phasellus vitae libero. Mauris ac felis sollicitudin quis, accumsan nisl risus, consequat id, wisi. Integer id dui. Pellentesque laoreet molestie turpis non augue. Sed sit amet, tempor varius, quam eu elementum diam leo tristique tempus id, eleifend posuere cubilia Curae, Cras congue, velit lectus felis, nec erat. Sed in enim. Duis eu mollis luctus tellus non ante. Ut consequat lacus rhoncus eu, ornare id, wisi. Donec porttitor risus. Sed in vehicula convallis viverra, dui a quam tempus enim non augue. Lorem ipsum a nunc. Nunc molestie sed, ultrices lorem nonummy a, blandit eros, sagittis ultricies. Nullam fermentum pede, posuere cubilia Curae, Sed in.",
         "tags":[  
            "sit",
            "liberum",
            "eros",
            "dolor",
            "sit",
            "amet"
         ]
      },
      {  
         "text":"Donec lectus est ullamcorper ut, pellentesque accumsan. Nullam accumsan. Proin justo. Vestibulum quam. Sed metus. Integer eget ipsum ut nibh sagittis lectus lectus, viverra diam mollis pulvinar. Morbi tincidunt. Proin nonummy eget, condimentum sed, dapibus vitae, facilisis nibh, fringilla at, nibh. Ut libero lacinia eget, dui. Morbi nibh. Maecenas eget ipsum primis in erat eu wisi. Aliquam ultricies pretium. Fusce interdum. Nulla facilisi. Morbi orci. Mauris ut tortor. Maecenas imperdiet sed, suscipit wisi. Suspendisse gravida. Pellentesque ac lacus. In laoreet fermentum. Morbi quam in nunc. Suspendisse vitae convallis diam ac nunc. Etiam hendrerit tellus tortor, fermentum sed, ullamcorper ac, pede. Curabitur et ultrices posuere nisl ut sem. Sed aliquet blandit, enim eu wisi. Suspendisse potenti. Quisque urna. Nam diam lorem, id ipsum. Vestibulum scelerisque. Duis neque tortor id ligula. Nunc mollis, orci orci, sodales.",
         "tags":[  
            "magnis",
            "vitae"
         ]
      },
      {  
         "text":"Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc vitae lorem sit amet, est. Aliquam orci. Nullam aliquet vulputate, odio nec augue. Vivamus sed wisi vel massa non sapien. Quisque ornare eu, ornare nisl, sollicitudin justo. Phasellus tempor varius, nisl urna luctus a, fermentum odio. Morbi bibendum. In hac habitasse platea dictumst. Proin posuere. Vestibulum ante ipsum dolor sit amet dolor. In hac habitasse platea dictumst. Quisque euismod non, tincidunt quis, massa. Maecenas viverra quis, commodo ante. Morbi hendrerit. Donec enim quis massa volutpat enim sed eros. In hac habitasse platea dictumst. Quisque lorem pretium vitae, arcu. Praesent feugiat. Donec id lorem lectus, volutpat tempus id, congue eleifend. Nulla posuere. Quisque condimentum. Donec at libero. Aliquam nisl. Nam ac felis. Cum sociis natoque.",
         "tags":[  
            "cum",
            "vitae",
            "lorem"
         ]
      },
      {  
         "text":"Vestibulum dapibus, ultricies lobortis laoreet diam. Etiam mollis neque ut turpis. Fusce quam fermentum sem luctus et lacus eu mi porttitor laoreet venenatis consequat. Curabitur quis ante. Morbi justo. Nulla dolor id eros. In justo orci mauris non erat volutpat. Maecenas eu felis. Pellentesque nonummy velit nulla ligula porttitor ullamcorper, enim sed dolor fermentum odio. Etiam urna tellus quis massa id tincidunt enim. Sed adipiscing dui aliquam vehicula eget, orci. Mauris vitae tellus. Nulla aliquet quis, tellus. Proin in dolor sit amet mauris ac arcu. Donec nonummy auctor vel, arcu. In arcu congue ac, laoreet viverra semper et, accumsan vitae, consequat tortor, pretium eget, bibendum vitae, vestibulum nec, iaculis massa. Duis hendrerit sollicitudin. Nulla aliquet ultrices et, neque. Class aptent taciti sociosqu ad.",
         "tags":[  
            "dapibus",
            "cum",
            "magnis"
         ]
      },
      {  
         "text":"Suspendisse rhoncus aliquam pharetra velit in dolor urna, eu mauris. Aenean congue risus. Sed feugiat ac, ante. Maecenas vitae erat id lacus rhoncus wisi. Phasellus pulvinar felis, feugiat venenatis, nunc odio at arcu. Aenean dictum commodo, tincidunt et, placerat porta tellus tortor, fermentum mi, id aliquet in, dapibus vel, urna. Vestibulum consectetuer adipiscing metus. Etiam vestibulum suscipit rutrum. Donec mauris pulvinar risus. Nulla gravida vel, semper quis, eleifend lectus nulla eu nulla dictum a, fermentum malesuada. Suspendisse est dolor, scelerisque a, mauris. Aenean ac enim quis blandit risus auctor mi. Morbi quis neque. Vestibulum elit laoreet hendrerit nonummy. Sed congue, aliquet quis, accumsan placerat eget, pede. Pellentesque nunc. Suspendisse elit. Mauris convallis nisl. Nam ullamcorper fringilla, vulputate imperdiet, risus ut mi. Cras volutpat aliquam.",
         "tags":[  
            "lex",
            "dapibus",
            "rhoncus"
         ]
      },
      {  
         "text":"Pellentesque eu tempor vitae, vulputate mi, nec magna hendrerit laoreet. Aenean nonummy dui lectus ut magna. In lacus lacus feugiat pede. Duis pretium eget, volutpat massa mattis adipiscing. Mauris nec enim ac lacinia ut, sagittis vel, eros. Mauris suscipit id, ornare tortor massa non neque ultrices sapien facilisis eget, suscipit dui eu libero. Nulla nec turpis cursus wisi, tempor varius, quam elit rhoncus ut, eleifend vel, accumsan sit amet neque vel ipsum erat, fringilla condimentum, pulvinar diam vel neque in neque. Sed fringilla lorem, pellentesque accumsan. Vestibulum ante at est. Vivamus consequat faucibus, dolor sit amet quam. Praesent vitae lacinia varius eu.",
         "tags":[  
            "vitae",
            "tempor",
            "lex",
            "elementum"
         ]
      },
      {  
         "text":"Sed pulvinar sit amet, vulputate vitae, nunc. Vestibulum vulputate. Morbi urna dapibus risus nec tellus. Integer ac felis. Nullam et magna. Suspendisse molestie. Quisque placerat porttitor. Maecenas blandit eu, luctus et interdum ligula libero, egestas consequat non, placerat dui. Pellentesque habitant morbi tristique in, mollis orci. Donec eget elit est, dapibus vel, orci. Sed venenatis. Sed dignissim vitae, faucibus orci orci, gravida tempor, tortor lacus a dolor non mattis at, convallis justo. Phasellus vestibulum lorem sodales in, augue. Nam consectetuer at, lacus. Vestibulum cursus justo non ipsum. Fusce sed orci orci, blandit quis, ipsum. Nulla malesuada. Ut tempus ac, tempus leo non ullamcorper et, tristique bibendum nulla. Fusce nisl at tortor. In ultricies leo. Aliquam erat velit et interdum velit lectus varius commodo vehicula, tortor metus hendrerit libero. Duis sed massa metus et magnis dis parturient montes, nascetur ridiculus mus. Fusce mollis luctus et odio. Morbi pede. Sed et netus.",
         "tags":[  
            "liberum",
            "eros",
            "urna",
            "vitae"
         ]
      },
      {  
         "text":"In hac habitasse platea dictumst. Nunc in dui. In hac habitasse platea dictumst. Vestibulum dapibus, libero posuere sed, neque. In hac habitasse platea dictumst. Nunc mollis, purus laoreet diam. Morbi massa vel ipsum at quam quam congue tellus, elementum at, pretium eget, molestie a, convallis accumsan, libero ornare velit eleifend lectus blandit iaculis, dui nulla, egestas sit amet, consectetuer adipiscing iaculis. Sed sagittis ac, felis. Pellentesque mattis at, lacus. Nullam sit amet sapien mauris viverra ligula. Nunc a dolor. Duis elementum diam vitae massa a enim ac ligula. Fusce consequat vel, orci. Vestibulum ante ipsum sed neque. Suspendisse eu tincidunt.",
         "tags":[  
            "platea",
            "lex",
            "lorem",
            "ipsum",
            "dolor",
            "sit"
         ]
      },
      {  
         "text":"Aliquam fermentum gravida ullamcorper wisi id felis. Mauris ut massa. Suspendisse rhoncus interdum, lacus. Integer condimentum magna orci elit, pulvinar vulputate adipiscing. Cum sociis natoque penatibus et libero. Pellentesque habitant morbi tristique vitae, facilisis est porta neque. Nulla et metus. Aenean ac sapien. Cras dolor ut nulla. Etiam ullamcorper, enim non imperdiet facilisis nunc, rhoncus a, adipiscing dui convallis quam at tellus non metus sem, sed nulla. Morbi tincidunt. Praesent odio nec elit est, dapibus ac, vehicula ut, tincidunt tellus tristique commodo. Cras faucibus a, elementum vitae, congue quis, bibendum id, condimentum faucibus turpis. Donec nulla in neque. Fusce nisl nulla ultricies a, posuere cubilia Curae, Sed porttitor, quam in fermentum turpis libero, id tortor orci elit, interdum viverra. Pellentesque et ultrices nec, arcu. Donec enim quis viverra a, blandit venenatis, nunc.",
         "tags":[  
            "lex",
            "lorem",
            "ipsum",
            "dolor",
            "sit",
            "magnis",
            "cursus"
         ]
      },
      {  
         "text":"Ut sed turpis. Praesent commodo wisi. Phasellus vestibulum. Cras magna urna mattis feugiat quam porta lorem, tempus nunc. Etiam in interdum wisi dui gravida diam. Pellentesque fringilla turpis. Praesent ac turpis egestas. Cras in volutpat enim vel laoreet a, mattis lectus at orci luctus ut, lectus. Praesent odio quis neque. Mauris mattis ac, augue. Morbi quam accumsan vestibulum. Etiam ut aliquet lacinia at, nibh. Ut rhoncus eu, eleifend placerat. Duis vel odio eget gravida sodales. Aenean facilisis dui quis massa non enim vulputate at, lacus. Phasellus ultrices, velit et quam. Donec ullamcorper fringilla, vulputate molestie. Phasellus vulputate mi, id felis. Nulla facilisi. Etiam blandit suscipit, dolor in consequat ipsum feugiat quam in faucibus turpis. Donec porttitor vitae, congue sit amet interdum velit.",
         "tags":[  
            "dolor",
            "sit",
            "amet",
            "urna",
            "vitae"
         ]
      },
      {  
         "text":"Quisque vitae felis in nulla orci consequat sed, eros. Vivamus imperdiet purus feugiat tortor venenatis nulla. Morbi facilisis libero. Aliquam nec adipiscing mauris. Pellentesque molestie sagittis. Curabitur condimentum velit. Duis dictum. Curabitur volutpat ultricies lobortis laoreet vel, massa. Integer magna auctor auctor vel, urna. Aenean lacus et magnis dis parturient montes, nascetur ridiculus mus. Nam cursus, lacus pede, molestie vitae, lorem. Etiam sapien eget egestas accumsan. Cras ut augue. Fusce venenatis blandit sed, congue tristique. Donec id erat vel mauris magna, at adipiscing elit. Aenean urna ullamcorper quam. Aliquam erat libero, facilisis pharetra. Cras magna ultrices interdum. Suspendisse dapibus eu.",
         "tags":[  
            "cursus",
            "lorem",
            "lex",
            "vitae",
            "felis"
         ]
      },
      {  
         "text":"Aliquam tempus enim. Donec sit amet mauris dui ornare in, accumsan eget, ante. Pellentesque malesuada augue nec tempor magna. Sed ac pede id mi ipsum non metus nisl, commodo ac, eleifend pede turpis velit, rhoncus libero et magnis dis parturient montes, nascetur ridiculus mus. Nunc accumsan luctus, nisl a posuere sit amet, vestibulum nec, molestie mauris. Nullam justo. Aliquam urna. Donec a arcu. Duis neque lorem, id rutrum et, ultricies lacinia erat. Vivamus lacus. Duis hendrerit risus. Aliquam eget metus. Proin sodales. Aenean ipsum ante, vitae arcu vitae felis fermentum diam elementum in, suscipit lectus. Curabitur vel massa sit amet dolor. Ut tempus arcu. Suspendisse dapibus aliquam. Nunc gravida. Suspendisse porttitor ante mollis neque vitae erat velit tristique eget, aliquet eget, sagittis luctus, ante ipsum primis in nunc. Phasellus blandit, dui non pede interdum ligula ut metus in vehicula libero.",
         "tags":[  
            "leo",
            "felis",
            "sit",
            "dolor"
         ]
      },
      {  
         "text":"Quisque ornare dapibus. Aenean nonummy auctor dignissim. Pellentesque porta tincidunt. Nullam in tortor mauris, interdum velit non eros porttitor auctor, sapien varius nec, sagittis lacus. Nulla quis massa quis nibh eu odio. Aenean ipsum orci, aliquam sapien. Fusce porttitor, arcu iaculis nec, pede. Pellentesque eget volutpat lacinia arcu elit, dictum ante. Curabitur enim. Maecenas mi risus ante et netus et turpis. Cras luctus nulla eu sem rutrum magna et gravida wisi id felis. Fusce tristique, augue at lacus magna, tincidunt congue. Praesent ac arcu vel massa metus nunc, fringilla non, feugiat dui, eget leo sodales turpis, accumsan sit amet libero posuere egestas volutpat. Ut blandit, quam. Proin lacus. Pellentesque tincidunt quis, interdum rhoncus, dui imperdiet quis, varius nunc, nonummy nunc vitae est diam.",
         "tags":[  
            "ipsum",
            "elementum",
            "vitae"
         ]
      },
      {  
         "text":"Etiam commodo, tincidunt in, dolor. Fusce suscipit mauris. Pellentesque habitant morbi tristique purus. Aenean scelerisque sem. In nec magna. Curabitur urna sit amet erat. Pellentesque scelerisque in, accumsan eget, wisi. Suspendisse eu eros. Nullam sed fermentum pellentesque adipiscing vitae, dapibus tellus. Ut eu nisl. Vestibulum ante in faucibus volutpat, erat volutpat. Nullam nec tellus rutrum in, ante. Praesent scelerisque porttitor lectus orci, viverra vel, lorem. Pellentesque dolor accumsan fringilla aliquet. Quisque rutrum, urna sem tincidunt sapien. Curabitur fringilla sollicitudin, urna et velit justo sem, vulputate accumsan. Nullam luctus, quam eu viverra diam elementum quis, aliquam enim molestie aliquam. Nunc molestie.",
         "tags":[  
            "ipsum",
            "elementum",
            "vitae",
            "cum",
            "felis"
         ]
      },
      {  
         "text":"Cras lorem quam sem, sed felis non felis. Nunc varius vehicula. Aliquam vitae ornare tellus. In hac habitasse platea dictumst. Quisque nunc. Vestibulum metus bibendum nulla quis ante. Pellentesque ante. Maecenas pellentesque sapien. Morbi aliquam id, imperdiet convallis. Integer leo ultrices condimentum et, elementum quis, congue ac, accumsan sit amet augue. Fusce iaculis, diam magna neque, vitae felis. Vestibulum id odio consequat lacus scelerisque neque ut nulla quis leo. Vivamus posuere cubilia Curae, Duis mauris nec diam eu cursus tristique, urna quam, lobortis vitae, cursus dolor urna mattis faucibus. Sed hendrerit sed, rutrum et, felis. Duis ipsum. Aenean mollis sodales. Phasellus id tortor. Maecenas at justo. Vestibulum laoreet sit amet dolor. In hac habitasse platea dictumst. Suspendisse ut metus. Curabitur tincidunt wisi, aliquam cursus wisi magna, fermentum eu, tempus erat consectetuer adipiscing elit. Quisque facilisis enim. Duis condimentum quam. Ut.",
         "tags":[  
            "felis",
            "cursus",
            "vitae",
            "elementum",
            "lex"
         ]
      },
      {  
         "text":"Nulla congue. Lorem ipsum primis in ipsum. Nam convallis viverra, enim id leo sed leo. Integer sit amet tempus et, fermentum pede, at libero. Vestibulum quam. Maecenas in massa. Proin venenatis pede. Vestibulum quis arcu. Cras tortor et est dolor, porttitor auctor, ante et ultrices nec, imperdiet quis, varius leo. In quam eu tincidunt hendrerit nonummy. Class aptent taciti sociosqu ad litora torquent per inceptos hymenaeos. Sed bibendum. Morbi nibh condimentum velit. Praesent a libero vel purus. Integer a leo et malesuada dolor. Vestibulum et magnis dis parturient montes, nascetur ridiculus mus. Mauris ultrices. Vestibulum id justo semper feugiat. Proin nunc libero, accumsan imperdiet, risus libero, ultricies iaculis pede id ultrices sit amet, massa. Mauris ornare nulla ut nisl. Nulla venenatis vitae, ultricies quis.",
         "tags":[  
            "leo",
            "felis",
            "sit",
            "vitae"
         ]
      }
   ]
       };
       service.getArticles = function(){
           console.log("Article service");
           return articles.articles;
       }
       return service;
    };
    
    angular.module('tagscloud').factory('ArticleService', ArticleService);
})();